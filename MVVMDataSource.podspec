#
#  Be sure to run `pod spec lint MVVMDataSource.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.

Pod::Spec.new do |spec|
  spec.name         = 'MVVMDataSource'
  spec.version      = '1.2.0'
  spec.summary      = 'Simple to use Model View ViewModel DataSource.'

  spec.description  = 'Simple to use Model View ViewModel DataSource for collection views and table views. For use with custom object datamodels or CoreData.'

  spec.homepage     = 'https://github.com/WAMF/MVVMDataSource.git'
  spec.license      = { :type => 'MIT', :file => 'LICENSE' }

  spec.author             = { 'Lee Higgins' => 'lee@wearemobilefirst.com' }
  spec.social_media_url   = "http://twitter.com/depthperpixel"

  spec.ios.deployment_target   = '10.0'
  spec.tvos.deployment_target  = '10.0'

  spec.source        = { :git => 'https://github.com/WAMF/MVVMDataSource.git', :tag => spec.version }

  spec.source_files  = 'MVVMDataSource/**/*.{swift}'
  spec.swift_version = '5.0'
end
