//
//  CoreDataExampleViewController.swift
//  MVVMDatasource
//
//  Created by Lee Higgins on 28/08/2017.
//  Copyright © 2017 WeAreMobileFirst. All rights reserved.
//

import UIKit
import CoreData
import MVVMDataSource

extension ExampleCoreDataProduct: ModelObject {} // add to model objects types

extension ExampleCoreDataProduct {
    static func randomProduct(context: NSManagedObjectContext) -> NSManagedObject {
        let i = arc4random_uniform(10)
        let object = ExampleCoreDataProduct(entity: ExampleCoreDataProduct.entity(), insertInto: context)
        object.name = "Product " + UUID().uuidString
        object.desc = "D " + String(i)
        object.price = NSDecimalNumber(decimal: Decimal(floatLiteral: 250.0 + (Double(i) * 100.0)))
        return object
    }
}

/*
 README:
 
 To demo the flexibility of the system we add 2 core data sections (powered by a sorted predicate)
 And 2 normal non coredata sections to the same datasource
 
 For best practice this VC should have its own ViewModel which creates the datasource etc,
 but since this is a demo for the datasource we do this directly in the VC to keep it simple
 */

class CoreDataExampleViewController: UIViewController, MVVMMapDataSourceDelegate {
    
    private var dataSource:MVVMMapCoreDataSource? = nil
    @IBOutlet weak var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        populateDataBaseIfNeeded()
    
        dataSource = createDatasource()
        dataSource?.delegate = self
        
        tableView?.dataSource = dataSource
        tableView?.estimatedRowHeight = 84
        tableView?.rowHeight = UITableView.automaticDimension
    }
    
    func createDatasource() -> MVVMMapCoreDataSource {
        
        //For best practice: get some data model objects using a service/repo via a use-case
        //here we just create an example
        //and populate our datasource
        
        let cheapSection = PredicateSection(sectionObject: "Cheap Section", entityName: ExampleCoreDataProduct.entity().name!, predicate:NSPredicate(format: "price <= 500"), sortDescriptors:[NSSortDescriptor.init(key: "price", ascending: true)],context: self.coreDataViewContext)
        
        let expensiveSection = PredicateSection(sectionObject: "Expensive Section", entityName: ExampleCoreDataProduct.entity().name!, predicate:NSPredicate(format: "price > 500"), sortDescriptors: [NSSortDescriptor.init(key: "price", ascending: true)],context: self.coreDataViewContext)
        
        
        let combinedCoreDataAndNormalObjects = [cheapSection,expensiveSection] + ExampleViewModelData.exampleData() //mix and match coredata with normal objects
        
        //Examples of datasource actions
        //for best practice:
        //the datamodel mutations should be done in a use case called via the view model (in this demo we do them here)
        //actions should come back to the VC for navigation changes but the VC should not have any logic, just perform segue etc...
        
        let newDataSource = MVVMMapCoreDataSource(sectionObjects:combinedCoreDataAndNormalObjects)
        
        let mutateAction = MVVMMapDataSourceAction( { [weak self] (indexPath, viewModel, modelObject) in
            if let context = self?.coreDataViewContext {
                if let producObject = modelObject as? ExampleCoreDataProduct {
                    producObject.price = NSDecimalNumber(decimal:Decimal(floatLiteral: (1000.0 * drand48()).rounded()))
                    try? context.save()
                }
            }
        })
        
        let removeAction = MVVMMapDataSourceAction( { [weak self] (indexPath, viewModel, modelObject) in
            if let context = self?.coreDataViewContext {
                if let managedObject = modelObject as? NSManagedObject{
                    context.delete(managedObject)
                    try? context.save()
                }
            }
        })
        
        let navAction = MVVMMapDataSourceAction( { [weak self] (indexPath, viewModel, modelObject) in
            self?.navigationController?.popViewController(animated: true)
        })
        
        newDataSource.register(viewModel: ExampleProductCoreDataViewModel(mutateAction: mutateAction,
                                                                        removeAction: removeAction,
                                                                        navAction: navAction),
                             forModel: ExampleCoreDataProduct.self)
        
        newDataSource.register(viewModel: ExampleProductSimpleViewModel(),forModel: ExampleProduct.self)
        
        newDataSource.register(viewModel: TableViewSectionViewModel(), forModel: String.self) // register a view model to handle the sections
        
        return newDataSource
    }
    
    func dataSourceWasMutated(_ datasource: MVVMMapBaseDataSource) {
        tableView?.reloadData()
    }
    
    func dataSourceUpperBoundAccessed(_ datasource: MVVMMapBaseDataSource, at indexPath: IndexPath) {
        print("Datasource high Bounds accessed for \(indexPath)... (now is a good point to fetch more data)")
    }
    
    func dataSourceLastItemAccessed(_ datasource: MVVMMapBaseDataSource) {
        print("Datasource last item accessed")
    }
    
    // some ugly stuff to demo realtime updating (do this in your dataModel layer.....)
    
    @IBAction func didTapAddButton(_ sender: Any) {
        let context = self.coreDataViewContext
        _ = ExampleCoreDataProduct.randomProduct(context: context)
        try? context.save()
    }
    
    @IBAction func didTapRemoveButton(_ sender: Any) {
        // remove a random object
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ExampleCoreDataProduct")
        fetchRequest.sortDescriptors = [NSSortDescriptor.init(key: "name", ascending: true)]
        let context = self.coreDataViewContext
        if let objects = try? context.fetch(fetchRequest)
        {
            if let anyManagedObject = objects.first as? NSManagedObject {
                context.delete(anyManagedObject)
                try? context.save()
            }
        }
    }
    
    func populateDataBaseIfNeeded() {
        // add some test data
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ExampleCoreDataProduct")
        fetchRequest.sortDescriptors = [NSSortDescriptor.init(key: "name", ascending: true)]
        let context = self.coreDataViewContext
        if let objects = try? context.fetch(fetchRequest)
        {
            if (objects.count == 0) {
                for _ in 0...10 {
                    _ = ExampleCoreDataProduct.randomProduct(context: context)
                }
                try? context.save()
            }
        }
    }
}
