//
//  TableViewSectionViewModel.swift
//  MVVMDatasource
//
//  Created by Lee Higgins on 25/08/2017.
//  Copyright © 2017 WeAreMobileFirst. All rights reserved.
//

import UIKit
import MVVMDataSource

struct TableViewSectionViewModel: BasicSectionViewModel, ViewModel {
   
    private var identifier: String = "TableViewSectionViewModel"
    var sectionHeaderTitle:String? = nil
    var sectionFooterTitle:String? = nil
    
    mutating func populateUsing(modelObject: ModelObject, indexPath: IndexPath?) {
        if let sectionTitle = modelObject as? String {
            sectionHeaderTitle = sectionTitle
            sectionFooterTitle = nil
        }
    }
    
    func reuseIdentifier() -> String {
        return identifier
    }
    
}
