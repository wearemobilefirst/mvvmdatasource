//
//  ExampleViewModelData.swift
//  MVVMDataSource
//
//  Created by Lee Higgins on 04/09/2017.
//  Copyright © 2017 WeAreMobileFirst. All rights reserved.
//

import UIKit
import MVVMDataSource

extension String:ModelObject {} // Add to our "allowed" Model objects

class ExampleViewModelData: NSObject {
    
    static func exampleData() -> [Section] {
        let appleProducts = [ExampleProduct(name: "iPad", desc: "A11 processor, 3Gb ram", price: 899.99),
                             ExampleProduct(name: "iPod", desc: "A11 processor, 1Gb ram", price: 299.99),
                             ExampleProduct(name: "iPhone 7", desc: "A10 processor, 2Gb ram", price: 899.99),
                             ExampleProduct(name: "iPhone 8", desc: "A11 processor, 3Gb ram", price: 899.99),
                             ExampleProduct(name: "iGlasses", desc: "A15 processor, 8Gb ram", price: 1899.99),
                             ExampleProduct(name: "iCar", desc: "Tesla P200 powertrain, 500km range", price: 49999.99)
        ]
        
        let microsoftProducts = [ExampleProduct(name: "Surface", desc: "Why?", price: 899.99),
                                 ExampleProduct(name: "Windows 8", desc: "Why?", price: 299.99),
                                 ExampleProduct(name: "Windows 10", desc: "Plays games", price: 899.99),
                                 ExampleProduct(name: "Holo lens", desc: "Magic", price: 899.99)
        ]
        return [Section(sectionObject: "Apple",rows: appleProducts),
                Section(sectionObject: "Microsoft",rows: microsoftProducts)]
    }

}
