//
//  ExampleProductTableViewCell.swift
//  MVVMDatasource
//
//  Created by Lee Higgins on 25/08/2017.
//  Copyright © 2017 WeAreMobileFirst. All rights reserved.
//

import UIKit
import MVVMDataSource

class ExampleProductTableViewCell: UITableViewCell, ViewModelView  {
    
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    @IBOutlet weak var marketingLabel: UILabel?
    @IBOutlet weak var removeActionButton: UIButton?
    @IBOutlet weak var mutateActionButton: UIButton?
    @IBOutlet weak var navActionButton: UIButton?
    private var removeActionBlock: MVVMMapDataSourceAction.BoundAction? = nil
    private var mutateActionBlock: MVVMMapDataSourceAction.BoundAction? = nil
    private var navActionBlock: MVVMMapDataSourceAction.BoundAction? = nil
    
    func populateUsing(viewModel: ViewModel) {
        if let exampleProductViewModel = viewModel as? ExampleProductSimpleViewModel {
            titleLabel?.text = exampleProductViewModel.mainTitle
            descriptionLabel?.text = exampleProductViewModel.subTitle
            marketingLabel?.text = exampleProductViewModel.marketingPrice
            removeActionBlock = nil
            mutateActionBlock = nil
            navActionBlock = nil
        }
        else if let exampleProductCoreDataViewModel = viewModel as? ExampleProductCoreDataViewModel {
            titleLabel?.text = exampleProductCoreDataViewModel.mainTitle
            descriptionLabel?.text = exampleProductCoreDataViewModel.subTitle
            marketingLabel?.text = exampleProductCoreDataViewModel.marketingPrice
            removeActionBlock = exampleProductCoreDataViewModel.removeAction
            mutateActionBlock = exampleProductCoreDataViewModel.mutateAction
            navActionBlock = exampleProductCoreDataViewModel.navAction
        }
        removeActionButton?.addTarget(self, action: #selector(removeDidTap(sender:)), for: UIControl.Event.touchUpInside)
        mutateActionButton?.addTarget(self, action: #selector(mutateDidTap(sender:)), for: UIControl.Event.touchUpInside)
        navActionButton?.addTarget(self, action: #selector(navDidTap(sender:)), for: UIControl.Event.touchUpInside)
    }
    
    @IBAction func removeDidTap(sender: UIButton) {
         removeActionBlock?()   // really only a wrapper to allow a block to be injected for the button press
    }
    
    @IBAction func mutateDidTap(sender: UIButton) {
         mutateActionBlock?()
    }
    
    @IBAction func navDidTap(sender: UIButton) {
        navActionBlock?()
    }
    
    
}
