//
//  ViewController.swift
//  MVVMDatasourceExample
//
//  Created by Lee Higgins on 25/08/2017.
//  Copyright © 2017 WeAreMobileFirst. All rights reserved.
//

import UIKit
import MVVMDataSource

class SimpleExampleViewController: UIViewController, MVVMMapDataSourceDelegate, UITableViewDelegate {
    
    private var dataSource:MVVMMapDataSource? = nil
    @IBOutlet weak var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let data = ExampleViewModelData.exampleData()
        
        dataSource = MVVMMapDataSource(sectionObjects: data)
        
        dataSource?.register(viewModel: ExampleProductSimpleViewModel(), forModel: ExampleProduct.self)
        dataSource?.register(viewModel: TableViewSectionViewModel(), forModel: String.self) // register a view model to handle the sections
        
        dataSource?.delegate = self
        tableView?.dataSource = dataSource
        tableView?.estimatedRowHeight = 84
        tableView?.rowHeight = UITableView.automaticDimension
    }

    func dataSourceWasMutated(_ datasource: MVVMMapBaseDataSource) {
        tableView?.reloadData()
    }
    
    func dataSourceUpperBoundAccessed(_ datasource: MVVMMapBaseDataSource, at indexPath: IndexPath) {
        print("Datasource high Bounds accessed for \(indexPath)... (now is a good point to fetch more data)")
    }
    
    func dataSourceLastItemAccessed(_ datasource: MVVMMapBaseDataSource) {
        print("Datasource last item accessed")
    }

}

