//
//  ExampleProduct.swift
//  MVVMDatasource
//
//  Created by Lee Higgins on 25/08/2017.
//  Copyright © 2017 WeAreMobileFirst. All rights reserved.
//

import UIKit
import Foundation
import MVVMDataSource

struct ExampleProduct {
    var name = ""
    var desc = ""
    var price:Decimal = 0
    
    init(name: String,desc: String,price: Decimal) {
        self.name = name
        self.desc = desc
        self.price = price
    }
}

extension ExampleProduct:ModelObject {} // Add to our "allowed" Model objects
