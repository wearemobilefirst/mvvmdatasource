//
//  ExampleProductSimpleViewModel.swift
//  MVVMDatasource
//
//  Created by Lee Higgins on 25/08/2017.
//  Copyright © 2017 WeAreMobileFirst. All rights reserved.
//

import UIKit
import MVVMDataSource

// Some made up transforming of model data to display (view) data
// we trim and construct Strings as an example

struct ExampleProductSimpleViewModel: ViewModel {
    
    private var identifier: String = "ExampleProductDefaultReuse"
    var mainTitle: String = ""
    var subTitle: String = ""
    
    var marketingPrice: String = ""
    var price:Decimal = 0
    
    private let descriptionCharLimit = 10
    
    mutating func populateUsing(modelObject: ModelObject, indexPath: IndexPath?) {
        guard let product = modelObject as? ExampleProduct else {return}
        mainTitle = product.name
        if product.desc.count > descriptionCharLimit {
            let endIndex = product.desc.index(product.desc.startIndex, offsetBy: descriptionCharLimit)
            subTitle = String(product.desc[..<endIndex])
        }
        else
        {
            subTitle = product.desc
        }
        price = product.price
        marketingPrice = "Only " + String(describing: price)
    }
    
    func reuseIdentifier() -> String {
        return identifier
    }
    
}
