//
//  MVVMCoreDataStack.swift
//  MVVMDatasource
//
//  Created by Lee Higgins on 28/08/2017.
//  Copyright © 2017 WeAreMobileFirst. All rights reserved.
//

import UIKit
import CoreData

public class CoreDataStack {
    
    static let globalStack: CoreDataStack = CoreDataStack(name: "AppDataModel")
    
    private var containerName: String
    
    init(name: String)
    {
        self.containerName = name
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: self.containerName)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
}


public extension UIViewController {
    
    var coreDataViewContext: NSManagedObjectContext {
        get {
            return CoreDataStack.globalStack.persistentContainer.viewContext
        }
    }
}
