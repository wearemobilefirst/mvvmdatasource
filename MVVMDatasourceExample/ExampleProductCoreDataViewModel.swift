//
//  ExampleProductCoreDataViewModel.swift
//  MVVMDatasource
//
//  Created by Lee Higgins on 28/08/2017.
//  Copyright © 2017 WeAreMobileFirst. All rights reserved.
//

import UIKit


import UIKit
import MVVMDataSource

// Some made up transforming of model data to display (view) data
// we trim and construct Strings as an example

struct ExampleProductCoreDataViewModel: ViewModel {

    private var identifier: String = "CoreDataExampleProductDefaultReuse"
    var mainTitle: String = ""
    var subTitle: String = ""
    
    var marketingPrice: String = ""
    var price: Decimal = 0
    
    var mutateAction: MVVMMapDataSourceAction.BoundAction?
    var removeAction: MVVMMapDataSourceAction.BoundAction?
    var navAction: MVVMMapDataSourceAction.BoundAction?
    
    private let _mutateAction: MVVMMapDataSourceAction?    // these are the viewModel Actions they are populated when model object is bound
    private let _removeAction: MVVMMapDataSourceAction?
    private let _navAction: MVVMMapDataSourceAction?
    
    private let descriptionCharLimit = 10
    private var actionIndex:IndexPath? = nil
    
    init (mutateAction: MVVMMapDataSourceAction?, removeAction: MVVMMapDataSourceAction?, navAction: MVVMMapDataSourceAction?) {
        self._mutateAction = mutateAction
        self._removeAction = removeAction
        self._navAction = navAction
    }
    
    mutating func populateUsing(modelObject: ModelObject, indexPath:IndexPath?)  {
        guard let product = modelObject as? ExampleCoreDataProduct else { return }
        mainTitle = product.name ?? "no title"
        if let desc = product.desc {
            if desc.count > descriptionCharLimit {
                let endIndex = desc.index(desc.startIndex, offsetBy: descriptionCharLimit)
                subTitle =  String(desc[..<endIndex]) + "..."
            }
            else
            {
                subTitle = desc
            }
        }
        price = (product.price as Decimal?) ?? Decimal(0.0)
        marketingPrice = "Only " + String(describing:price)
        if let indexPath = indexPath {
            mutateAction = _mutateAction?.actionFor(indexPath, viewModel: self, modelObject: modelObject)
            removeAction = _removeAction?.actionFor(indexPath, viewModel: self, modelObject: modelObject)
            navAction  = _navAction?.actionFor(indexPath, viewModel: self, modelObject: modelObject)
        }
    }
    
    func reuseIdentifier() -> String {
        return identifier
    }
    
}
