// swift-tools-version:4.2
import PackageDescription

let package = Package(
    name: "MVVMDataSource",
    products: [
        .library(name: "MVVMDataSource", targets: ["MVVMDataSource"]),
    ],
    targets: [
        .target(name: "MVVMDataSource", path: "MVVMDataSource"),
    ]
)