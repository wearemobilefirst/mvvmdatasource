//
//  MessageItemDatasource.swift
//  chat_concept
//
//  Created by Lee Higgins on 27/06/2017.
//  Copyright © 2017 We Are Mobile First. All rights reserved.
//
import Foundation
import UIKit

public protocol MVVMMapDataSourceDelegate {
    func dataSourceLastItemAccessed(_ datasource: MVVMMapBaseDataSource)
    func dataSourceUpperBoundAccessed(_ datasource: MVVMMapBaseDataSource, at indexPath: IndexPath)
    func dataSourceWasMutated(_ datasource: MVVMMapBaseDataSource)
    func dataSourceWillBindViewModel(_ datasource: MVVMMapBaseDataSource,viewModel: inout ViewModel, at indexPath: IndexPath)
    func dataSourceDidBindViewModel(_ datasource: MVVMMapBaseDataSource,viewModel: inout ViewModel, at indexPath: IndexPath)
}

public extension MVVMMapDataSourceDelegate {
    func dataSourceLastItemAccessed(_ datasource: MVVMMapBaseDataSource) {}
    func dataSourceUpperBoundAccessed(_ datasource: MVVMMapBaseDataSource, at indexPath: IndexPath) {} //make optional
    func dataSourceWillBindViewModel(_ datasource: MVVMMapBaseDataSource,viewModel: inout ViewModel, at indexPath: IndexPath) {}  //make optional
    func dataSourceDidBindViewModel(_ datasource: MVVMMapBaseDataSource,viewModel: inout ViewModel, at indexPath: IndexPath) {} //make optional
}

public protocol BasicSectionViewModel {
    var sectionHeaderTitle: String? { get }
    var sectionFooterTitle: String? { get }
}

public typealias MVVMPair = (model: Any.Type,viewModel: ViewModel)

public class Section {
    
    private var rowObjects: [Any] = []
    var sectionObject: Any?
    
    public init(sectionObject: Any?,rows: [Any]){
        self.sectionObject = sectionObject
        self.rowObjects = rows
    }
    
    var rows: [Any] {
        get {
            return rowObjects
        }
        set (rows) {
           rowObjects = rows
        }
    }
}

// helper used to inject indexpath into a plain block to be added as a view action
public class MVVMMapDataSourceAction {
    public typealias Action = (_ indexPath: IndexPath, _ viewModel: ViewModel, _ modelObject: Any) -> ()
    public typealias BoundAction = () -> ()
    
    private var action: Action?
    
    public init(_ action:@escaping Action)
    {
        self.action = action
    }
    
    public func actionFor(_ indexPath: IndexPath, viewModel: ViewModel, modelObject: ModelObject) -> BoundAction {
        return { [weak self]() in
            self?.action?(indexPath, viewModel, modelObject)
        }
    }
}

public class MVVMMapBaseDataSource: NSObject {
    
    public enum AccessTriggerLimits: Float {
        case max = 1.0
        case min = 0.0
    }
    public var delegate:MVVMMapDataSourceDelegate?
    public var upperBoundAccessTrigger: Float = AccessTriggerLimits.max.rawValue
    private var upperBoundAccessed: [Int : Bool] = [:]
    
    private var mvvmMap:[MVVMPair] = [] // here we hold a map of what model objects use what view models
    
    public func register(viewModel: ViewModel, forModel: Any.Type) {
        mvvmMap.append(MVVMPair(forModel,viewModel))
    }
    
    public func viewModelObject(for modelObject: ModelObject, at indexPath: IndexPath) -> ViewModel? {
        
        if let viewModelObject = modelObject as? ViewModel { // support for ViewModelDataSources
            return viewModelObject
        }
        
        for mvvm in mvvmMap
        {
            if areEqualTypes(modelClass: mvvm.model,object: modelObject) {
                var viewModel = mvvm.viewModel
                willBind(viewModel: &viewModel, at: indexPath)
                viewModel.populateUsing(modelObject: modelObject, indexPath: indexPath)
                didBind(viewModel: &viewModel, at: indexPath)
                return viewModel
            }
        }
        return nil
    }
    
    internal func areEqualTypes(modelClass: Any.Type, object: Any) -> Bool {
        return (modelClass == type(of: object))
    }
    
    public func modelForIndexPath(indexPath: IndexPath) -> ModelObject? {
        return nil
    }
    
    public func sectionData(sectionIndex: Int) -> ModelObject? {
        return nil
    }
    
    public func sectionCount() -> Int {
        return 0
    }
    
    public func rowCount(section: Int) -> Int {
        return 0
    }
    
    internal func willBind(viewModel: inout ViewModel, at indexPath: IndexPath)
    {
        delegate?.dataSourceWillBindViewModel(self, viewModel: &viewModel, at: indexPath)
    }
    
    internal func didBind(viewModel: inout ViewModel, at indexPath: IndexPath)
    {
        delegate?.dataSourceDidBindViewModel(self, viewModel: &viewModel, at: indexPath)
        triggerUpperBoundAccessIfNeeded(at: indexPath)
    }

    public func notifyMutation(){
        delegate?.dataSourceWasMutated(self)
        resetUpperBoundsTrigger()
    }
    
    public func resetUpperBoundsTrigger() {
        upperBoundAccessed = [:]
    }
    
    private func triggerUpperBoundAccessIfNeeded(at indexPath: IndexPath) {
        guard indexPath.section < sectionCount() else {
            return
        }
        let row = indexPath.row
        let section = indexPath.section
        let previouslyAccessed = upperBoundAccessed[section] ?? false
        let trigger = max(AccessTriggerLimits.min.rawValue,min(AccessTriggerLimits.max.rawValue,upperBoundAccessTrigger))
        if row >= Int(Float(max(0, rowCount(section: section) - 1)) * trigger),
            !previouslyAccessed {
            delegate?.dataSourceUpperBoundAccessed(self, at: indexPath)
            upperBoundAccessed[indexPath.section] = true
        }
        if section == max(0, sectionCount() - 1) {
            if row == max(0, rowCount(section: section) - 1){
                delegate?.dataSourceLastItemAccessed(self)
            }
        }
    }
}

public class MVVMMapDataSource: MVVMMapBaseDataSource {

    internal var sectionObjects: [Section]
    
    public init(sectionObjects: [Section]) {
        self.sectionObjects = sectionObjects
    }
    
    public convenience init(rowObjects: [Any]) {
        self.init(sectionObjects: [Section(sectionObject: "",rows: rowObjects)])
    }
    
    override public func sectionData(sectionIndex: Int) -> ModelObject? {
       return self.sectionObjects[sectionIndex].sectionObject as? ModelObject
    }
    
    override public func sectionCount() -> Int {
        return self.sectionObjects.count
    }
    
    override public func rowCount(section: Int) -> Int {
        guard section < sectionObjects.count else {
            return 0
        }
        return self.sectionObjects[section].rows.count
    }
    
    override public func modelForIndexPath(indexPath: IndexPath ) -> ModelObject? {
        if (indexPath.section < sectionObjects.count)
        {
            let section = sectionObjects[indexPath.section]
            if (indexPath.row < section.rows.count) {
                return sectionObjects[indexPath.section].rows[indexPath.row] as? ModelObject
            }
        }
        return nil
    }
    
    public func addRowObject(modelObject: Any, section: Int) -> Bool {
        return addRowObjects(objects: [modelObject], section: section)
    }
    
    public func addRowObjects(objects: [Any], section: Int) -> Bool {
        if(section < sectionObjects.count && section >= 0) {
            var rows = sectionObjects[section].rows
            rows += objects
            sectionObjects[section].rows = rows
            notifyMutation()
            return true
        }
        return false
    }
    
    public func addSectionObject(sectionObject: Section) {
       addSectionObjects(objects: [sectionObject])
    }
    
    public func addSectionObjects(objects: [Section]) {
        sectionObjects += objects
        notifyMutation()
    }
    
    public func removeSectionObject(section: Int) {
        guard section < sectionObjects.count else {
            return
        }
        sectionObjects.remove(at: section)
        notifyMutation()
    }
    
    public func lastSectionObject() -> Section? {
        return sectionObjects.last
    }
    
    public func firstSectionObject() -> Section? {
        return sectionObjects.first
    }
    
    public func lastObject() -> Any? {
        if let lastSection = sectionObjects.last {
            return lastSection.rows.last
        }
        return nil
    }
    
    public func firstObject() -> Any? {
        if let firstSection = sectionObjects.first {
            return firstSection.rows.first
        }
        return nil
    }
    
    public func section(at indexPath: IndexPath) -> Section? {
        let section = indexPath.section
        guard section < sectionObjects.count else {
            return nil
        }
        return sectionObjects[section]
    }
    
    public func totalItemCount() -> Int {
        return sectionObjects.reduce(0) { $0 + $1.rows.count }
    }

}

extension MVVMMapBaseDataSource: UITableViewDataSource {
    
    public func updateVisibleCells(_ tableView: UITableView) {
        if let visibleCells = tableView.visibleCells as? [ViewModelView]
        {
            for cell in visibleCells {
                if let tableViewCell = cell as? UITableViewCell {
                    if let indexPath = tableView.indexPath(for: tableViewCell) {
                        update(view: cell, indexPath: indexPath)
                    }
                }
            }
        }
    }

    public func update(view: ViewModelView, indexPath: IndexPath) {
        if let modelObject = modelForIndexPath(indexPath: indexPath)
        {
            if let viewModel = viewModelObject(for: modelObject, at: indexPath) {
                view.populateUsing(viewModel: viewModel)
            }
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowCount(section: section)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let modelObject = modelForIndexPath(indexPath: indexPath)
        if let model = modelObject
        {
            if let viewModel = viewModelObject(for: model, at: indexPath) {
                if let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.reuseIdentifier()) {
                    if let viewModelCell =  cell as? ViewModelView {
                        viewModelCell.populateUsing(viewModel: viewModel)
                    }
                    return cell
                }
            }
        }
        let defaultCell = UITableViewCell(style: .default, reuseIdentifier: "UITableViewCell")
        defaultCell.textLabel?.text = "Unable to render \(String(describing: modelObject))"
        defaultCell.backgroundView?.backgroundColor = UIColor.red
        return defaultCell
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return sectionCount()
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let indexPath = IndexPath(row: 0,section: section)
        if let sectionObject = sectionData(sectionIndex: section) {
            if let viewModel = viewModelObject(for: sectionObject, at: indexPath) as? BasicSectionViewModel {
                return viewModel.sectionHeaderTitle
            }
        }
        return nil
    }
    
    public func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        let indexPath = IndexPath(row: 0,section: section)
        if let sectionObject = sectionData(sectionIndex: section) {
            if let viewModel = viewModelObject(for: sectionObject, at: indexPath) as? BasicSectionViewModel {
                return viewModel.sectionFooterTitle
            }
        }
        return nil
    }
}

extension MVVMMapBaseDataSource: UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return rowCount(section: section)
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let modelObject = modelForIndexPath(indexPath: indexPath) {
            if let viewModel = viewModelObject(for: modelObject,at: indexPath) {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: viewModel.reuseIdentifier(), for: indexPath)
                if let viewModelCell =  cell as? ViewModelView {
                    viewModelCell.populateUsing(viewModel: viewModel)
                }
                return cell
            }
        }
        return UICollectionViewCell()
    }
    
    public func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if let sectionObject = sectionData(sectionIndex: indexPath.section)
        {
            if let viewModel = viewModelObject(for: sectionObject,at: indexPath) {
                let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: viewModel.reuseIdentifier(), for: indexPath)
                if let viewModelView = view as? ViewModelView {
                    viewModelView.populateUsing(viewModel: viewModel)
                }
                return view
            }
        }
        return UICollectionReusableView()
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sectionCount()
    }
}
