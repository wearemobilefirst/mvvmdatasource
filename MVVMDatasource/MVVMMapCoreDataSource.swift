//
//  MVVMMapCoreDataSource.swift
//  MVVMDatasource
//
//  Created by Lee Higgins on 28/08/2017.
//  Copyright © 2017 WeAreMobileFirst. All rights reserved.
//

import UIKit
import CoreData

public class PredicateSection: Section {
    
    private var sectionFRC:NSFetchedResultsController<NSFetchRequestResult>
    
    public override var rows:[Any] {
        get {
            return sectionFRC.fetchedObjects ?? []
        }
        set(rows) {
            return
        }
    }
    
    public init(sectionObject: Any?, entityName: String, predicate: NSPredicate, sortDescriptors: [NSSortDescriptor], context: NSManagedObjectContext)
    {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.predicate = predicate
        request.sortDescriptors = sortDescriptors
        sectionFRC = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        try? sectionFRC.performFetch()
        super.init(sectionObject: sectionObject, rows: [])
    }
    
    fileprivate func setChangeDelegate(delegate: NSFetchedResultsControllerDelegate){
        sectionFRC.delegate = delegate
    }
}

public class MVVMMapCoreDataSource: MVVMMapDataSource, NSFetchedResultsControllerDelegate {
    
    public override init(sectionObjects: [Section]) {
        super.init(sectionObjects: sectionObjects)
        for section in self.sectionObjects {
            if let predicateSection = section as? PredicateSection {
                predicateSection.setChangeDelegate(delegate: self)
            }
        }
    }
    
    public override func addSectionObject(sectionObject: Section) {
        super.addSectionObject(sectionObject: sectionObject)
        if let predicateSection = sectionObject as? PredicateSection {
             predicateSection.setChangeDelegate(delegate: self)
        }
    }
    
    public override func addRowObject(modelObject: Any, section: Int) -> Bool {
        if(section < sectionObjects.count && section >= 0) {
            let sectionObject = sectionObjects[section]
            if sectionObject is PredicateSection {
                return false // you can´t add a normal object to a predicate section
            }
            else
            {
                return super.addRowObject(modelObject: modelObject, section: section)
            }
        }
        return false
    }
    
    //LH we have to do a fudge here as the type of managed objects don´t match generated classes!
    internal override func areEqualTypes(modelClass: Any.Type, object: Any) -> Bool {
        if let managedObject = object as? NSManagedObject {
            return (modelClass == managedObject.classForCoder)
        }
        return super.areEqualTypes(modelClass: modelClass,object: object)
    }
    
    public func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        notifyMutation()
    }
    
    
}
