//
//  MVVMDatasource.h
//  MVVMDatasource
//
//  Created by Lee Higgins on 24/08/2017.
//  Copyright © 2017 WeAreMobileFirst. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MVVMDatasource.
FOUNDATION_EXPORT double MVVMDatasourceVersionNumber;

//! Project version string for MVVMDatasource.
FOUNDATION_EXPORT const unsigned char MVVMDatasourceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MVVMDatasource/PublicHeader.h>

