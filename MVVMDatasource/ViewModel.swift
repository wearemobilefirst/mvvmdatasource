//
//  ViewModel.swift
//  chat_concept
//
//  Created by Lee Higgins on 27/06/2017.
//  Copyright © 2017 We Are Mobile First. All rights reserved.
//

import UIKit

// could lock down with associated types

public protocol ModelObject {
    
}

public protocol ViewModelView {
    func populateUsing(viewModel:ViewModel) // Used to bind a viewModel to a view (populate a view)
}

public protocol ViewModel {
    func reuseIdentifier() -> String
    mutating func populateUsing(modelObject: ModelObject, indexPath: IndexPath?) // Used to bind the viewModel to the Model object (populate from the model)
}

public extension ViewModel {
    mutating func populateUsing(modelObject: ModelObject) {
        populateUsing(modelObject: modelObject, indexPath: nil)
    }
}
